import os
import shutil

ABS_PATH = os.path.abspath(os.curdir)

if __name__ == '__main__':
    for dir, sub_dirs, names_files in os.walk(ABS_PATH):
        if dir == ABS_PATH:
            continue
        for name_file in names_files:
            os.rename(
                dir + '\\' + name_file,
                ABS_PATH + '\\' + dir.replace(ABS_PATH, '')[1:].replace('\\', '_') + '_' + name_file
            )

    for dir in os.listdir(ABS_PATH):
        if os.path.isdir(dir):
            shutil.rmtree(dir, ignore_errors=False)
