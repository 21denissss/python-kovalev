import numpy as np

if __name__ == '__main__':
    generate_number = np.random.randint(low=0, high=100)
    while True:
        user_number = int(input('Введите число: '))
        if user_number > generate_number:
            print(f'Загаданное число меньше, чем {user_number}.')
        elif user_number < generate_number:
            print(f'Загаданное число больше, чем {user_number}.')
        else:
            print(f'Угадали, число действительно было {generate_number}.')
            break
